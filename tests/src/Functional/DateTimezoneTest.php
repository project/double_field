<?php

declare(strict_types=1);

namespace Drupal\Tests\double_field\Functional;

use Drupal\Component\Utility\NestedArray;
use Drupal\double_field\Plugin\Field\FieldFormatter\Base as BaseFormatter;

/**
 * A test for date timezone calculations.
 *
 * @group double_field
 */
final class DateTimezoneTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $storage_settings['storage']['first']['type'] = 'datetime_iso8601';
    $storage_settings['storage']['first']['datetime_type'] = 'datetime';
    $storage_settings['storage']['second']['type'] = 'datetime_iso8601';
    $storage_settings['storage']['second']['datetime_type'] = 'date';

    $this->saveFieldStorageSettings($storage_settings);

    /** @var \Drupal\Core\Entity\Entity\EntityViewDisplay $view_display */
    $view_display = \Drupal::entityTypeManager()
      ->getStorage('entity_view_display')
      ->load("node.{$this->contentTypeId}.default");

    $settings['first']['format_type'] = 'html_datetime';
    $settings['second']['format_type'] = 'html_datetime';
    $options = [
      'label' => 'hidden',
      'type' => 'double_field_unformatted_list',
      'settings' => NestedArray::mergeDeep(BaseFormatter::defaultSettings(), $settings),
      'third_party_settings' => [],
    ];
    $view_display->setComponent($this->fieldName, $options);
    $view_display->save();
  }

  /**
   * Test callback.
   */
  public function testDateSubfield(): void {

    $edit = [
      'title[0][value]' => 'Example',
      $this->fieldName . '[0][first][date]' => '2020-01-18',
      $this->fieldName . '[0][first][time]' => '03:12:00',
      $this->fieldName . '[0][second][date]' => '2020-01-15',
    ];
    $this->drupalGet('node/add/' . $this->contentTypeId);
    $this->submitForm($edit, 'Save');

    // -- Test formatter.
    $xpath = <<< 'XPATH'
      //div[@class = "double-field-first"]
      /time[@datetime = "2020-01-18T03:12:00Z" and text() = "2020-01-18T03:12:00+0300"]
      XPATH;
    $this->assertXpath($xpath);

    $xpath = <<< 'XPATH'
      //div[@class = "double-field-second"]
      /time[@datetime = "2020-01-15T15:00:00Z" and text() = "2020-01-15T12:00:00+0000"]
      XPATH;
    $this->assertXpath($xpath);

    $this->drupalGet('node/1/edit');

    // -- Test widget.
    $xpath = <<< XPATH
      //fieldset[contains(@class, "double-field-elements")]
      //div[@id = "edit-$this->fieldName-0-first" and position() = 1]
      /div[position() = 1]/input[@type = "date" and @value = "2020-01-18"]
      /../..
      /div[position() = 2]/input[@type = "time" and @value = "03:12:00"]
      XPATH;
    $this->assertXpath($xpath);
    $xpath = <<< XPATH
      //fieldset[contains(@class, "double-field-elements")]
      //div[@id = "edit-$this->fieldName-0-second" and position() = 2]
      /div[position() = 1]/input[@type = "date" and @value = "2020-01-15"]
      XPATH;
    $this->assertXpath($xpath);

    // Date only field should not have time element.
    $xpath = <<< XPATH
      //div[contains(@class, "double-field-elements")]
      /div[@id = "edit-$this->fieldName-0-second" and position() = 2]
      //input[@type = "time"]
      XPATH;
    $this->assertNoXpath($xpath);
  }

}
