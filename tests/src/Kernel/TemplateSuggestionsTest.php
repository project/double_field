<?php

declare(strict_types=1);

namespace Drupal\double_field\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the theme suggestions hooks function.
 *
 * @group double_field
 */
final class TemplateSuggestionsTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'double_field',
  ];

  /**
   * Test callback.
   */
  public function testTemplateHookSuggestions(): void {

    $variables = [
      'elements' => ['#field_name' => 'field_example'],
    ];
    $module_handler = $this->container->get('module_handler');

    $suggestions = $module_handler->invokeAll('theme_suggestions_double_field_item', [$variables]);
    self::assertSame(['double_field_item__field_example'], $suggestions);

    $suggestions = $module_handler->invokeAll('theme_suggestions_double_field_definition_list', [$variables]);
    self::assertSame(['double_field_definition_list__field_example'], $suggestions);

    $suggestions = [];
    $data = ['context' => []];
    $module_handler->alter('theme_suggestions_item_list', $suggestions, $data);
    self::assertSame([], $suggestions);

    $suggestions = [];
    $data = [
      'context' => ['double_field' => ['field_name' => 'field_example']],
    ];
    $module_handler->alter('theme_suggestions_item_list', $suggestions, $data);
    self::assertSame(['item_list__double_field__field_example'], $suggestions);

    $suggestions = [];
    $data = [];
    $module_handler->alter('theme_suggestions_table', $suggestions, $data);
    self::assertSame([], $suggestions);

    $suggestions = [];
    $data['attributes']['double-field--field-name'] = 'field_example';
    $module_handler->alter('theme_suggestions_table', $suggestions, $data);
    self::assertSame(['table__double_field__field_example'], $suggestions);

    $suggestions = [];
    $data = [];
    $module_handler->alter('theme_suggestions_details', $suggestions, $data);
    self::assertSame([], $suggestions);

    $suggestions = [];
    $data = [
      'element' => [
        '#attributes' => ['double-field--field-name' => 'field_example'],
      ],
    ];
    $module_handler->alter('theme_suggestions_details', $suggestions, $data);
    self::assertSame(['details__double_field__field_example'], $suggestions);
  }

}
